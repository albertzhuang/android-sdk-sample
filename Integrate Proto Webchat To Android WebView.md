# How To Integrate Proto Webchat To Android WebView


## Create a WebView in Android



*   Create `/asset` folder inside `/app` folder of project
*   Create an `index.html` file inside that folder



![alt_text](images/image1.png "image_tooltip")

*   Create a WebChatActivity to load that `index.html` content:



![alt_text](images/image2.png "image_tooltip")

*   Copy the configuration script from your Proto console to the `index.html`:  



![alt_text](images/image3.png "image_tooltip")

*   Adding `Proto.openWindow()` before `Proto.show()` to automatically load the chat screen in fullscreen mode.


## Calling Android API from Webchat



*   We can call Android API by two ways:
1. Call Android API directly from WebView by creating a JavascriptInterface bridge 
2. Use Deep Link in the URL format and intercept the URL redirection event every time users click on that link to call the Android API we want
*   Create a WebAppInterface class with the Android API method you want to call and add JavascriptInterface annotation before method’s name.  



![alt_text](images/image4.png "image_tooltip")

*   For example, we will show a Toast message whenever users close the chat screen (by clicking on X button). To do that, we add the Proto’s `onWindowClosed` callback in the `index.html` file. We call the Android API `showToast()` and fire a Deep Link event `“proto://close”` at the same time:



![alt_text](images/image5.png "image_tooltip")

*   Now, in the WebViewActivity we need to register this JavascriptInterface (section 2) and override the URL loading to handle the Deep Link (section 1) like in the figure below.



![alt_text](images/image6.png "image_tooltip")

*   **Important:** in order to save the chat history list and be able to resume the connection after the application restores from the background state, we must enable **domStorageEnabled**.


## Send Proto’s Trigger from WebView and handle the Bot message in the Deep Link format



*   Create a Trigger API in Proto console, for example: show_toast



![alt_text](images/image7.png "image_tooltip")

*   Create a Bot’s response message for that trigger in the Deep Link format, for example: `proto://showtoast`


![alt_text](images/image8.png "image_tooltip")

*   Now, in Android WebViewActivity, add the below code to send the trigger from WebView and handle the `showtoast` action.



![alt_text](images/image9.png "image_tooltip")


![alt_text](images/image10.png "image_tooltip")
