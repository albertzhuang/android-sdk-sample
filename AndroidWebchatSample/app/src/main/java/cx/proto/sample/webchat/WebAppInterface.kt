package cx.proto.sample.webchat

import android.content.Context
import android.webkit.JavascriptInterface
import android.widget.Toast


class WebAppInterface(val context: Context) {

    /** Show a toast from the web page  */
    @JavascriptInterface
    fun showToast(toast: String?) {
        Toast.makeText(context, toast, Toast.LENGTH_SHORT).show()
    }

}