# How To Integrate Proto Webchat To iOS WebView


## Create a WebView in iOS



*   Create `/asset` folder inside `/app` folder of project
*   Create an `index.html` file inside that folder



![alt_text](images-ios/image1.png "image_tooltip")

*   Create a ViewController to load that `index.html` content, and make our ViewController conform to WKNavigationDelegate, WKScriptMessageHandler protocols in order to do two-way communication between iOS webview and javascript.



![alt_text](images-ios/image2.png "image_tooltip")

*   Copy the configuration script from your Proto console to the `index.html`:  



![alt_text](images-ios/image3.png "image_tooltip")

*   Adding `Proto.openWindow()` before `Proto.show()` to automatically load the chat screen in fullscreen mode.
*   Adding this line to `<head>` section make WebView load in fullscreen mode in iOS:
```
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,viewport-fit=cover"/>
```


## Calling iOS API from Webchat



*   We can call iOS API by two ways:
1. Call iOS API directly from WebView by creating a Javascript interface bridge 
2. Use Deep Link in the URL format and intercept the URL redirection event every time users click on that link to call the iOS API we want

*   Create a Javascript bridge by making our ViewController conform to the WKScriptMessageHandler protocol and override method `userContentController(_: didReceive:)`   



![alt_text](images-ios/image4.png "image_tooltip")

*   For example, we will show a Alert message whenever users close the chat screen (by clicking on X button). To do that, we add the Proto’s `onWindowClosed` callback in the `index.html` file. We call `window.webkit.messageHandlers.iOSInterface.postMessage` **OR** fire a Deep Link event `“proto://close”`:



![alt_text](images-ios/image5.png "image_tooltip")

*   Now, in the ViewController we need to override method `userContentController(_: didReceive:)` and call iOS `showAlert()` method. In case you use the Deep Link method, see the section below on how to do it.


## Send Proto’s Trigger from WebView and handle the Bot message in the Deep Link format



*   Create a Trigger API in Proto console, for example: show_toast



![alt_text](images-ios/image7.png "image_tooltip")

*   Create a Bot’s response message for that trigger in the Deep Link format, for example: `proto://showtoast`


![alt_text](images-ios/image8.png "image_tooltip")

*   Now, in iOS ViewController, add the below code to handle the trigger `showtoast` from WebView and call `showAlert()`.


![alt_text](images-ios/image10.png "image_tooltip")
